const fs = require('fs-extra');

// this represents a config file
const template1Data = {
	template: 'template1'
};

// data require to write to the new banksite
const metaData = {
	template: 'template-1',
	directory: '../banksite-templates/src/app/templates/template-1',
	lander: {
		path: 'template-1-lander/template-1-lander.component',
		className: 'Template1LanderComponent',
		selector: 'app-template-1-lander'
	},
	thankyou: {
		path: 'template-1-thankyou/template-1-thankyou.component',
		className: 'Template1ThankyouComponent',
		selector: 'app-template-1-thankyou'
	},
	checkout: {
		path: 'template-1-checkout/template-1-checkout.component',
		className: 'Template1CheckoutComponent',
		selector: 'app-template-1-checkout'
	}
}

function rewriteFile2(data) {
	const importLander = `import { ${metaData.lander.className} } from './${metaData.template}/${metaData.lander.path}';`;
	const importThankyou = `import { ${metaData.thankyou.className} } from './${metaData.template}/${metaData.thankyou.path}';`;
	const importCheckout = `import { ${metaData.checkout.className} } from './${metaData.template}/${metaData.checkout.path}';`;
	return `${importLander}
${importThankyou}
${importCheckout}
${data.toString()}`;
}

function addDeclarations2(data) {
	const declarations = /declarations: \[\s+AppComponent/g;
		const newDeclarations = `declarations: [
			AppComponent,
			${metaData.lander.className},
			${metaData.thankyou.className},
			${metaData.checkout.className}`;
		return data.toString().replace(declarations, newDeclarations);
}

function generateSiteWithRoutes(data) {
	let newBanksite = `../banksite1`;
	const newBanksiteAppModule = '../banksite1/src/app/app.module.ts';
	const bankSiteShell = '../banksite';

	const landerComponent = `${newBanksite}/src/app/lander/lander.component.html`;
	const thankyouComponent = `${newBanksite}/src/app/thankyou/thankyou.component.html`;
	const checkoutComponent = `${newBanksite}/src/app/checkout/checkout.component.html`;
	
	fs.ensureDir(newBanksite))
		.then(() => console.log('copying banksite'))
		.then(() => fs.copy(bankSiteShell, newBanksite)) // copy the shell directory contents over to the new one
		.then((dir) => fs.ensureDir(`${newBanksite}/src/app/${metaData.template}`)) // create the template directory
		.then(() => fs.copy(metaData.directory, `${newBanksite}/src/app/${metaData.template}`)) // copy the template director from the library over to the new banksite
		.then(() => fs.readFile(newBanksiteAppModule))
		.then((data) => rewriteFile2(data)) // add the template 1 import statement
		.then((data) => fs.writeFile(newBanksiteAppModule, data)) // rewrite the banksite module
		.then(() => fs.readFile(newBanksiteAppModule)) // read the root module again
		.then((data) => addDeclarations2(data)) // add the Template1Component to the declarations
		.then((data) => fs.writeFile(newBanksiteAppModule, data)) // rewrite the banksite module
		.then(() => fs.writeFile(landerComponent, `<${metaData.lander.selector}></${metaData.lander.selector}>`)) // add the template selector
		.then(() => fs.writeFile(thankyouComponent, `<${metaData.thankyou.selector}></${metaData.thankyou.selector}>`)) // add the template selector
		.then(() => fs.writeFile(checkoutComponent, `<${metaData.checkout.selector}></${metaData.checkout.selector}>`)) // add the template selector
		.then(() => console.log('done'))
		.catch(err => console.log(err));
};

generateSiteWithRoutes();
